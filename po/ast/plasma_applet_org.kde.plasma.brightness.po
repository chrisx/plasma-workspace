# Copyright (C) 2024 This file is copyright:
# This file is distributed under the same license as the plasma-workspace package.
#
# SPDX-FileCopyrightText: 2024 Enol P. <enolp@softastur.org>
msgid ""
msgstr ""
"Project-Id-Version: plasma-workspace\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2024-04-07 00:40+0000\n"
"PO-Revision-Date: 2024-03-16 22:34+0100\n"
"Last-Translator: Enol P. <enolp@softastur.org>\n"
"Language-Team: Asturian <alministradores@softastur.org>\n"
"Language: ast\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Lokalize 24.02.0\n"

#: package/contents/ui/BrightnessItem.qml:29
#, kde-format
msgctxt "Backlight on or off"
msgid "Off"
msgstr ""

#: package/contents/ui/BrightnessItem.qml:30
#, kde-format
msgctxt "Brightness level"
msgid "Low"
msgstr ""

#: package/contents/ui/BrightnessItem.qml:31
#, kde-format
msgctxt "Brightness level"
msgid "Medium"
msgstr ""

#: package/contents/ui/BrightnessItem.qml:32
#, kde-format
msgctxt "Brightness level"
msgid "High"
msgstr ""

#: package/contents/ui/BrightnessItem.qml:33
#, kde-format
msgctxt "Backlight on or off"
msgid "On"
msgstr ""

#: package/contents/ui/BrightnessItem.qml:45
#, kde-format
msgctxt "Placeholder is brightness percentage"
msgid "%1%"
msgstr "%1%"

#: package/contents/ui/KeyboardColorItem.qml:78
#, kde-format
msgid "Follow accent color"
msgstr ""

#: package/contents/ui/main.qml:68
#, kde-format
msgid "Brightness and Color"
msgstr "Brillu y color"

#: package/contents/ui/main.qml:80
#, kde-format
msgid "Screen brightness at %1%"
msgstr ""

#: package/contents/ui/main.qml:83
#, kde-format
msgid "Keyboard brightness at %1%"
msgstr ""

#: package/contents/ui/main.qml:87
#, kde-format
msgctxt "Status"
msgid "Night Light off"
msgstr ""

#: package/contents/ui/main.qml:89
#, kde-format
msgctxt "Status; placeholder is a temperature"
msgid "Night Light at %1K"
msgstr ""

#: package/contents/ui/main.qml:99
#, kde-format
msgid "Scroll to adjust screen brightness"
msgstr ""

#: package/contents/ui/main.qml:102
#, kde-format
msgid "Middle-click to toggle Night Light"
msgstr ""

#: package/contents/ui/main.qml:206
#, kde-format
msgctxt "@action:inmenu"
msgid "Configure Night Light…"
msgstr "Configurar la lluz nocherniega…"

#: package/contents/ui/NightColorItem.qml:75
#, kde-format
msgctxt "Night light status"
msgid "Off"
msgstr ""

#: package/contents/ui/NightColorItem.qml:78
#, kde-format
msgctxt "Night light status"
msgid "Unavailable"
msgstr "Nun ta disponible"

#: package/contents/ui/NightColorItem.qml:81
#, kde-format
msgctxt "Night light status"
msgid "Not enabled"
msgstr "Nun s'activó"

#: package/contents/ui/NightColorItem.qml:84
#, kde-format
msgctxt "Night light status"
msgid "Not running"
msgstr "Nun ta n'execución"

#: package/contents/ui/NightColorItem.qml:87
#, kde-format
msgctxt "Night light status"
msgid "On"
msgstr ""

#: package/contents/ui/NightColorItem.qml:90
#, kde-format
msgctxt "Night light phase"
msgid "Morning Transition"
msgstr ""

#: package/contents/ui/NightColorItem.qml:92
#, kde-format
msgctxt "Night light phase"
msgid "Day"
msgstr ""

#: package/contents/ui/NightColorItem.qml:94
#, kde-format
msgctxt "Night light phase"
msgid "Evening Transition"
msgstr ""

#: package/contents/ui/NightColorItem.qml:96
#, kde-format
msgctxt "Night light phase"
msgid "Night"
msgstr ""

#: package/contents/ui/NightColorItem.qml:107
#, kde-format
msgctxt "Placeholder is screen color temperature"
msgid "%1K"
msgstr "%1K"

#: package/contents/ui/NightColorItem.qml:142
#, kde-format
msgid "Configure…"
msgstr "Configurar…"

#: package/contents/ui/NightColorItem.qml:142
#, kde-format
msgid "Enable and Configure…"
msgstr "Activar y configurar…"

#: package/contents/ui/NightColorItem.qml:168
#, kde-format
msgctxt "Label for a time"
msgid "Transition to day complete by:"
msgstr ""

#: package/contents/ui/NightColorItem.qml:170
#, kde-format
msgctxt "Label for a time"
msgid "Transition to night scheduled for:"
msgstr ""

#: package/contents/ui/NightColorItem.qml:172
#, kde-format
msgctxt "Label for a time"
msgid "Transition to night complete by:"
msgstr ""

#: package/contents/ui/NightColorItem.qml:174
#, kde-format
msgctxt "Label for a time"
msgid "Transition to day scheduled for:"
msgstr ""

#: package/contents/ui/PopupDialog.qml:66
#, kde-format
msgid "Display Brightness"
msgstr "Brillu de la pantalla"

#: package/contents/ui/PopupDialog.qml:97
#, kde-format
msgid "Keyboard Brightness"
msgstr "Brillu del tecláu"

#: package/contents/ui/PopupDialog.qml:130
#, fuzzy, kde-format
#| msgid "Keyboard Brightness"
msgid "Keyboard Color"
msgstr "Brillu del tecláu"

#: package/contents/ui/PopupDialog.qml:140
#, kde-format
msgid "Night Light"
msgstr "Lluz nocherniega"
