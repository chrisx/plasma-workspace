# Copyright (C) 2024 This file is copyright:
# This file is distributed under the same license as the plasma-workspace package.
#
# SPDX-FileCopyrightText: 2024 Zayed Al-Saidi <zayed.alsaidi@gmail.com>
msgid ""
msgstr ""
"Project-Id-Version: plasma-workspace\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2024-04-05 00:39+0000\n"
"PO-Revision-Date: 2024-02-10 20:54+0400\n"
"Last-Translator: Zayed Al-Saidi <zayed.alsaidi@gmail.com>\n"
"Language-Team: ar\n"
"Language: ar\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=6; plural=n==0 ? 0 : n==1 ? 1 : n==2 ? 2 : n%100>=3 "
"&& n%100<=10 ? 3 : n%100>=11 && n%100<=99 ? 4 : 5;\n"
"X-Generator: Lokalize 23.08.1\n"

#: devicenotifications.cpp:278
#, kde-format
msgid "%1 has been plugged in."
msgstr "وُصِل %1."

#: devicenotifications.cpp:278
#, kde-format
msgid "A USB device has been plugged in."
msgstr "وُصِل جهاز يو اس بي."

#: devicenotifications.cpp:281
#, kde-format
msgctxt "@title:notifications"
msgid "USB Device Detected"
msgstr "اكتشف جهاز يو اس بي"

#: devicenotifications.cpp:299
#, kde-format
msgid "%1 has been unplugged."
msgstr "فُصل %1."

#: devicenotifications.cpp:299
#, kde-format
msgid "A USB device has been unplugged."
msgstr "فُصِل جهاز يو اس بي."

#: devicenotifications.cpp:302
#, kde-format
msgctxt "@title:notifications"
msgid "USB Device Removed"
msgstr "أزيل جهاز يو اس بي"
