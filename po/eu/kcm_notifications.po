# Translation for kcm_notifications.po to Euskara/Basque (eu).
# Copyright (C) 2019-2024 This file is copyright:
# This file is distributed under the same license as the plasma-workspace package.
# SPDX-FileCopyrightText: 2024 KDE euskaratzeko proiektuaren arduraduna <xalba@ni.eus>
#
# Translators:
# Iñigo Salvador Azurmendi <xalba@ni.eus>, 2019, 2020, 2021, 2022, 2023, 2024.
msgid ""
msgstr ""
"Project-Id-Version: plasma-workspace\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2024-03-22 00:39+0000\n"
"PO-Revision-Date: 2024-03-24 11:20+0100\n"
"Last-Translator: Iñigo Salvador Azurmendi <xalba@ni.eus>\n"
"Language-Team: Basque <kde-i18n-eu@kde.org>\n"
"Language: eu\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"
"X-Generator: Lokalize 24.02.1\n"

#: kcm.cpp:70
#, kde-format
msgid "Toggle do not disturb"
msgstr "Txandakatu «zaratarik ez»"

#: sourcesmodel.cpp:392
#, kde-format
msgid "Other Applications"
msgstr "Beste aplikazio batzuk"

#: ui/ApplicationConfiguration.qml:92
#, kde-format
msgid "Show popups"
msgstr "Erakutsi mezu gainerakorrak"

#: ui/ApplicationConfiguration.qml:106
#, kde-format
msgid "Show in do not disturb mode"
msgstr "Erakutsi «zaratarik ez» modua"

#: ui/ApplicationConfiguration.qml:119 ui/main.qml:183
#, kde-format
msgid "Show in history"
msgstr "Erakutsi historian"

#: ui/ApplicationConfiguration.qml:130
#, kde-format
msgid "Show notification badges"
msgstr "Erakutsi jakinarazpen txartelak"

#: ui/ApplicationConfiguration.qml:167
#, kde-format
msgctxt "@title:table Configure individual notification events in an app"
msgid "Configure Events"
msgstr "Konfiguratu gertaerak"

#: ui/ApplicationConfiguration.qml:175
#, kde-format
msgid ""
"This application does not support configuring notifications on a per-event "
"basis"
msgstr ""
"Aplikazio honek ez du onartzen gertaera bakoitzeko jakinarazpenak "
"konfiguratzea"

#: ui/ApplicationConfiguration.qml:269
#, kde-format
msgid "Show a message in a pop-up"
msgstr "Erakutsi mezu bat leiho-gainerako batean"

#: ui/ApplicationConfiguration.qml:278
#, kde-format
msgid "Play a sound"
msgstr "Jo soinu bat"

#: ui/ApplicationConfiguration.qml:289
#, kde-format
msgctxt "@info:tooltip"
msgid "Preview sound"
msgstr "Aurrez entzun soinua"

#: ui/ApplicationConfiguration.qml:315
#, kde-format
msgctxt "Reset the notification sound to a default one"
msgid "Reset"
msgstr "Berrezarri"

#: ui/ApplicationConfiguration.qml:327
#, kde-format
msgid "Choose sound file"
msgstr "Hautatu soinu-fitxategia"

#: ui/main.qml:46
#, kde-format
msgctxt "@action:button Plasma-specific notifications"
msgid "System Notifications…"
msgstr "Sistemaren jakinarazpenak..."

#: ui/main.qml:52
#, kde-format
msgctxt "@action:button Application-specific notifications"
msgid "Application Settings…"
msgstr "Aplikazio-ezarpenak..."

#: ui/main.qml:77
#, kde-format
msgid ""
"Could not find a 'Notifications' widget, which is required for displaying "
"notifications. Make sure that it is enabled either in your System Tray or as "
"a standalone widget."
msgstr ""
"Ezin izan du «Jakinarazpenak» trepeta bat aurkitu, eta jakinarazpenak  "
"erakusteko beharrezkoa da. Ziurtatu ezazu zure Sistemako erretiluan edo "
"trepeta autonomo gisa gaituta dagoela."

#: ui/main.qml:88
#, kde-format
msgctxt "Vendor and product name"
msgid "Notifications are currently provided by '%1 %2' instead of Plasma."
msgstr ""
"Jakinarazpenak une honetan Plasmaren ordez beste honek hornitzen ditu, '%1 "
"%2'."

#: ui/main.qml:92
#, kde-format
msgid "Notifications are currently not provided by Plasma."
msgstr "Jakinarazpenak une honetan ez ditu Plasmak hornitzen."

#: ui/main.qml:99
#, kde-format
msgctxt "@title:group"
msgid "Do Not Disturb mode"
msgstr "«Zaratarik ez» modua"

#: ui/main.qml:104
#, kde-format
msgctxt "Automatically enable Do Not Disturb mode when screens are mirrored"
msgid "Enable automatically:"
msgstr "Gaitu automatikoki:"

#: ui/main.qml:105
#, kde-format
msgctxt "Automatically enable Do Not Disturb mode when screens are mirrored"
msgid "When screens are mirrored"
msgstr "Pantailak ispilatuta daudenean"

#: ui/main.qml:117
#, kde-format
msgctxt "Automatically enable Do Not Disturb mode during screen sharing"
msgid "During screen sharing"
msgstr "Pantaila partekatu bitartean"

#: ui/main.qml:132
#, kde-format
msgctxt "Keyboard shortcut to turn Do Not Disturb mode on and off"
msgid "Manually toggle with shortcut:"
msgstr "Eskuz txandakatu lasterbidea erabiliz:"

#: ui/main.qml:139
#, kde-format
msgctxt "@title:group"
msgid "Visibility conditions"
msgstr "Ikusgaitasun baldintzak"

#: ui/main.qml:144
#, kde-format
msgid "Critical notifications:"
msgstr "Jakinarazpen larriak:"

#: ui/main.qml:145
#, kde-format
msgid "Show in Do Not Disturb mode"
msgstr "Erakutsi «zaratarik ez» moduan"

#: ui/main.qml:157
#, kde-format
msgid "Normal notifications:"
msgstr "Jakinarazpen arruntak:"

#: ui/main.qml:158
#, kde-format
msgid "Show over full screen windows"
msgstr "Pantaila osoko leihoen gainean erakutsi"

#: ui/main.qml:170
#, kde-format
msgid "Low priority notifications:"
msgstr "Lehentasun txikiko jakinarazpenak:"

#: ui/main.qml:171
#, kde-format
msgid "Show popup"
msgstr "Erakutsi mezu gainerakorra"

#: ui/main.qml:200
#, kde-format
msgctxt "@title:group As in: 'notification popups'"
msgid "Popups"
msgstr "Gainerakorrak"

#: ui/main.qml:206
#, kde-format
msgctxt "@label"
msgid "Location:"
msgstr "Kokalekua:"

#: ui/main.qml:207
#, kde-format
msgctxt "Popup position near notification plasmoid"
msgid "Near notification icon"
msgstr "Jakinarazpen ikonotik hurbil"

#: ui/main.qml:244
#, kde-format
msgid "Choose Custom Position…"
msgstr "Aukeratu zuk nahi duzun kokapena..."

#: ui/main.qml:253 ui/main.qml:269
#, kde-format
msgid "%1 second"
msgid_plural "%1 seconds"
msgstr[0] "segundo %1"
msgstr[1] "%1 segundo"

#: ui/main.qml:258
#, kde-format
msgctxt "Part of a sentence like, 'Hide popup after n seconds'"
msgid "Hide after:"
msgstr "Ezkutatu ... ondoren:"

#: ui/main.qml:281
#, kde-format
msgctxt "@title:group"
msgid "Additional feedback"
msgstr "Erreakzio osagarriak"

#: ui/main.qml:287
#, kde-format
msgctxt "Show application jobs in notification widget"
msgid "Show in notifications"
msgstr "Erakutsi jakinarazpenetan"

#: ui/main.qml:288
#, kde-format
msgid "Application progress:"
msgstr "Aplikazioaren aurrerapena:"

#: ui/main.qml:302
#, kde-format
msgctxt "Keep application job popup open for entire duration of job"
msgid "Keep popup open during progress"
msgstr "Eduki mezu gainerakorra irekita aurrera egin ahala"

#: ui/main.qml:315
#, kde-format
msgid "Notification badges:"
msgstr "Jakinarazpen txartelak:"

#: ui/main.qml:316
#, kde-format
msgid "Show in task manager"
msgstr "Erakutsi ataza kudeatzailean"

#: ui/PopupPositionPage.qml:14
#, kde-format
msgid "Popup Position"
msgstr "Mezu gainerakorren kokapena"

#: ui/SourcesPage.qml:20
#, kde-format
msgid "Application Settings"
msgstr "Aplikazio-ezarpenak"

#: ui/SourcesPage.qml:107
#, kde-format
msgid "Applications"
msgstr "Aplikazioak"

#: ui/SourcesPage.qml:108
#, kde-format
msgid "System Services"
msgstr "Sistemaren zerbitzuak"

#: ui/SourcesPage.qml:156
#, kde-format
msgid "No application or event matches your search term"
msgstr ""
"Ez dago zuk bilatutako terminoarekin bat datorren aplikazio edo gertaerarik"

#: ui/SourcesPage.qml:180
#, kde-format
msgid ""
"Select an application from the list to configure its notification settings "
"and behavior"
msgstr ""
"Hautatu aplikazio bat zerrendatik bere jakinarazpen ezarpenak eta jokabidea "
"konfiguratzeko"

#~ msgctxt "@action:button Application-specific notifications"
#~ msgid "Configure Application Settings…"
#~ msgstr "Konfiguratu aplikazio-ezarpenak..."

#~ msgctxt "@title:group"
#~ msgid "Application-specific settings"
#~ msgstr "Aplikazioaren berariazko ezarpenak"

#~ msgid "Configure…"
#~ msgstr "Konfiguratu..."

#~ msgctxt "Enable Do Not Disturb mode when screens are mirrored"
#~ msgid "Enable:"
#~ msgstr "Gaitu:"

#~ msgctxt "Keyboard shortcut to turn Do Not Disturb mode on and off"
#~ msgid "Keyboard shortcut:"
#~ msgstr "Teklatuko laster-tekla:"

#~ msgid "Configure Notifications"
#~ msgstr "Konfiguratu jakinarazpenak"

#~ msgid "This module lets you manage application and system notifications."
#~ msgstr ""
#~ "Modulu honek aplikazioen eta sistemaren jakinarazpenak kudeatzen uzten "
#~ "dizu."

#~ msgctxt "Turn do not disturb mode on/off with keyboard shortcut"
#~ msgid "Toggle with:"
#~ msgstr "Txandakatu honekin:"

#~ msgctxt "NAME OF TRANSLATORS"
#~ msgid "Your names"
#~ msgstr "Iñigo Salvador Azurmendi"

#~ msgctxt "EMAIL OF TRANSLATORS"
#~ msgid "Your emails"
#~ msgstr "xalba@ni.eus"

#~ msgid "Kai Uwe Broulik"
#~ msgstr "Kai Uwe Broulik"

#~ msgid "Show critical notifications"
#~ msgstr "Erakutsi jakinarazpen larriak"

#~ msgid "Always keep on top"
#~ msgstr "Beti gainean ipini"

#~ msgid "Popup position:"
#~ msgstr "Mezu gainerakorren kokapena:"
