# translation of kcm_soundtheme.po to Slovak
# SPDX-FileCopyrightText: 2023 Roman Paholík <wizzardsk@gmail.com>
msgid ""
msgstr ""
"Project-Id-Version: kcm_soundtheme\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2024-02-20 00:39+0000\n"
"PO-Revision-Date: 2023-12-09 09:38+0100\n"
"Last-Translator: Roman Paholik <wizzardsk@gmail.com>\n"
"Language-Team: Slovak <kde-sk@linux.sk>\n"
"Language: sk\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: Lokalize 23.08.3\n"
"Plural-Forms: nplurals=3; plural=(n==1) ? 0 : (n>=2 && n<=4) ? 1 : 2;\n"

#: kcm_soundtheme.cpp:111
#, kde-format
msgctxt "Name of the fallback \"freedesktop\" sound theme"
msgid "FreeDesktop"
msgstr "FreeDesktop"

#: kcm_soundtheme.cpp:112
#, kde-format
msgid "Fallback sound theme from freedesktop.org"
msgstr ""

#: ui/main.qml:29
#, kde-format
msgctxt "@option:check"
msgid "Enable notification sounds"
msgstr ""

#: ui/main.qml:96
#, kde-format
msgctxt ""
"%2 is a theme name or a list of theme names that the current theme inherits "
"from"
msgid "Based on: %2"
msgid_plural "Based on: %2"
msgstr[0] ""
msgstr[1] ""
msgstr[2] ""

#: ui/main.qml:109
#, kde-format
msgctxt ""
"@label Precedes a list of buttons which can be clicked to preview the "
"theme's sounds. Keep it very short"
msgid "Preview:"
msgstr ""

#: ui/main.qml:128
#, kde-format
msgctxt "@info:tooltip"
msgid "Preview sound \"%1\""
msgstr ""

#: ui/main.qml:150
#, kde-format
msgctxt "@info:tooltip"
msgid "Preview the demo sound for the theme \"%1\""
msgstr ""

#: ui/main.qml:185
#, kde-format
msgctxt "%1 is a sound theme name. %2 is a sound name"
msgid "The %1 theme doesn't include the sound '%2'"
msgstr ""

#: ui/main.qml:190
#, kde-format
msgid "Failed to preview sound: %1"
msgstr ""
