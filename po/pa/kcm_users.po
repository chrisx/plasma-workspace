# Copyright (C) 2023 This file is copyright:
# This file is distributed under the same license as the plasma-desktop package.
#
# SPDX-FileCopyrightText: 2020, 2021, 2023, 2024 A S Alam <aalam.yellow@gmail.com>
msgid ""
msgstr ""
"Project-Id-Version: plasma-desktop\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2024-02-24 00:38+0000\n"
"PO-Revision-Date: 2024-01-19 17:45-0600\n"
"Last-Translator: A S Alam <aalam@punlinux.org>\n"
"Language-Team: Punjabi <kde-i18n-doc@kde.org>\n"
"Language: pa\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Lokalize 23.08.4\n"

#: src/fingerprintmodel.cpp:137 src/fingerprintmodel.cpp:244
#, kde-format
msgid "No fingerprint device found."
msgstr "ਕੋਈ ਫਿੰਗਰਪਰਿੰਟ ਡਿਵਾਈਸ ਨਹੀਂ ਹੈ।"

#: src/fingerprintmodel.cpp:317
#, kde-format
msgid "Retry scanning your finger."
msgstr "ਆਪਣੀ ਉਂਗਲ ਨੂੰ ਮੁੜ ਸਕੈਨ ਕਰਨ ਦੀ ਕੋਸ਼ਿਸ਼ ਕਰੋ।"

#: src/fingerprintmodel.cpp:319
#, kde-format
msgid "Swipe too short. Try again."
msgstr "ਬਹੁਤ ਕਾਹਲੀ ਨਾਲ ਘਸਾਇਆ। ਫੇਰ ਕੋਸ਼ਿਸ਼ ਕਰੋ।"

#: src/fingerprintmodel.cpp:321
#, kde-format
msgid "Finger not centered on the reader. Try again."
msgstr "ਉਂਗਲ ਰੀਡਰ ਦੇ ਕੇਂਦਰ ਉੱਤੇ ਨਹੀਂ ਹੈ। ਫੇਰ ਕੋਸ਼ਿਸ਼ ਕਰੋ।"

#: src/fingerprintmodel.cpp:323
#, kde-format
msgid "Remove your finger from the reader, and try again."
msgstr "ਰੀਡਰ ਤੋਂ ਆਪਣੀ ਉਂਗਲ ਨੂੰ ਹਟਾਓ ਅਤੇ ਫੇਰ ਕੋਸ਼ਿਸ਼ ਕਰੋ।"

#: src/fingerprintmodel.cpp:331
#, kde-format
msgid "Fingerprint enrollment has failed."
msgstr "ਫਿੰਗਰ-ਪਰਿੰਟ ਦਾਖਲਾ ਅਸਫ਼ਲ ਹੈ।"

#: src/fingerprintmodel.cpp:334
#, kde-format
msgid ""
"There is no space left for this device, delete other fingerprints to "
"continue."
msgstr "ਇਸ ਡਿਵਾਈਸ ਉੱਤੇ ਕੋਈ ਖਾਲੀ ਥਾਂ ਨਹੀਂ ਬਚੀ ਹੈ, ਜਾਰੀ ਰੱਖਣ ਲਈ ਹੋਰ ਫਿੰਗਰ-ਪਰਿੰਟਾਂ ਨੂੰ ਹਟਾਓ।"

#: src/fingerprintmodel.cpp:337
#, kde-format
msgid "The device was disconnected."
msgstr "ਡਿਵਾਈਸ ਡਿਸ-ਕਨੈਕਟ ਸੀ।"

#: src/fingerprintmodel.cpp:342
#, kde-format
msgid "An unknown error has occurred."
msgstr "ਅਣਪਛਾਤੀ ਗਲਤੀ ਆਈ ਹੈ।"

#: src/ui/ChangePassword.qml:27 src/ui/UserDetailsPage.qml:170
#, kde-format
msgid "Change Password"
msgstr "ਪਾਸਵਰਡ ਬਦਲੋ"

#: src/ui/ChangePassword.qml:32
#, kde-format
msgid "Set Password"
msgstr "ਪਾਸਵਰਡ ਸੈੱਟ ਕਰੋ"

#: src/ui/ChangePassword.qml:55
#, kde-format
msgid "Password"
msgstr "ਪਾਸਵਰਡ"

#: src/ui/ChangePassword.qml:70
#, kde-format
msgid "Confirm password"
msgstr "ਪਾਸਵਰਡ ਤਸਦੀਕ ਕਰੋ"

#: src/ui/ChangePassword.qml:89 src/ui/CreateUser.qml:68
#, kde-format
msgid "Passwords must match"
msgstr "ਪਾਸਵਰਡ ਮਿਲਦੇ ਹੋਣੇ ਚਾਹੀਦੇ ਹਨ"

#: src/ui/ChangeWalletPassword.qml:16
#, kde-format
msgid "Change Wallet Password?"
msgstr "ਵਾਲਟ ਪਾਸਵਰਡ ਬਦਲਣਾ ਹੈ?"

#: src/ui/ChangeWalletPassword.qml:25
#, kde-kuit-format
msgctxt "@info"
msgid ""
"Now that you have changed your login password, you may also want to change "
"the password on your default KWallet to match it."
msgstr ""
"ਹੁਣ ਤੁਸੀਂ ਆਪਣਾ ਲਾਗਇਨ ਪਾਸਵਰਡ ਬਦਲ ਲਿਆ ਹੈ ਤਾਂ ਤੁਸੀਂ ਆਪਣੇ ਮੂਲ ਕੇ-ਵਾਲਟ ਲਈ ਵੀ ਪਾਸਵਰਡ ਨੂੰ ਮਿਲਦੇ "
"ਪਾਸਵਰਡ ਲਈ ਬਦਲ ਸਕਦੇ ਹੋ।"

#: src/ui/ChangeWalletPassword.qml:31
#, kde-format
msgid "What is KWallet?"
msgstr "ਕੇਵਾਲਿਟ ਕੀ ਹੈ?"

#: src/ui/ChangeWalletPassword.qml:41
#, kde-format
msgid ""
"KWallet is a password manager that stores your passwords for wireless "
"networks and other encrypted resources. It is locked with its own password "
"which differs from your login password. If the two passwords match, it can "
"be unlocked at login automatically so you don't have to enter the KWallet "
"password yourself."
msgstr ""
"ਕੇ-ਵਾਲਟ ਬੇਤਾਰ ਨੈੱਟਵਰਕ ਅਤੇ ਹੋਰ ਇੰਕ੍ਰਿਪਟ ਕੀਤੇ ਸੋਤਾਂ ਲਈ ਤੁਹਾਡੇ ਪਾਸਵਰਡ ਸੰਭਾਲਣ ਲਈ ਪਾਸਵਰਡ ਮੈਨੇਜਰ "
"ਹੈ। ਇਹ ਇਸ ਦੇ ਖੁਦ ਆਪਣੇ ਪਾਸਵਰਡ ਨਾਲ ਲਾਕ ਹੁੰਦਾ ਹੈ, ਜੋ ਤੁਹਾਡੇ ਲਾਗਇਨ ਪਾਸਵਰਡ ਤੋਂ ਵੱਖਰਾ ਹੁੰਦਾ ਹੈ। ਜੇ "
"ਦੋਵੇਂ ਪਾਸਵਰਡ ਮਿਲਦੇ ਹੋਣ ਤਾਂ ਇਹ ਲਾਗਇਨ ਹੋਣ ਉੱਤੇ ਆਪਣੇ-ਆਪ ਅਣ-ਲਾਕ ਹੋ ਜਾਂਦਾ ਹੈ ਤਾਂ ਕਿ ਤੁਹਾਨੂੰ ਕੇ-ਵਾਲਟ "
"ਲਈ ਖੁਦ ਪਾਸਵਰਡ ਨਹੀਂ ਭਰਨਾ ਪੈਂਦਾ ਹੈ।"

#: src/ui/ChangeWalletPassword.qml:58
#, kde-format
msgid "Change Wallet Password"
msgstr "ਵਾਲਿਟ ਪਾਸਵਰਡ ਬਦਲੋ"

#: src/ui/ChangeWalletPassword.qml:67
#, kde-format
msgid "Leave Unchanged"
msgstr "ਨਾ-ਬਦਲਿਆ ਹੀ ਛੱਡੋ"

#: src/ui/CreateUser.qml:15
#, kde-format
msgid "Create User"
msgstr "ਵਰਤੋਂਕਾਰ ਬਣਾਓ"

#: src/ui/CreateUser.qml:33 src/ui/UserDetailsPage.qml:132
#, kde-format
msgid "Name:"
msgstr "ਨਾਂ:"

#: src/ui/CreateUser.qml:37 src/ui/UserDetailsPage.qml:140
#, kde-format
msgid "Username:"
msgstr "ਵਰਤੋਂਕਾਰ-ਨਾਂ:"

#: src/ui/CreateUser.qml:47 src/ui/UserDetailsPage.qml:150
#, kde-format
msgid "Standard"
msgstr "ਸਟੈਂਡਰਡ"

#: src/ui/CreateUser.qml:48 src/ui/UserDetailsPage.qml:151
#, kde-format
msgid "Administrator"
msgstr "ਪਰਸ਼ਾਸ਼ਕ"

#: src/ui/CreateUser.qml:51 src/ui/UserDetailsPage.qml:154
#, kde-format
msgid "Account type:"
msgstr "ਖਾਤੇ ਦੀ ਕਿਸਮ:"

#: src/ui/CreateUser.qml:56
#, kde-format
msgid "Password:"
msgstr "ਪਾਸਵਰਡ:"

#: src/ui/CreateUser.qml:61
#, kde-format
msgid "Confirm password:"
msgstr "ਪਾਸਵਰਡ ਤਸਦੀਕ ਕਰੋ:"

#: src/ui/CreateUser.qml:78
#, kde-format
msgid "Create"
msgstr "ਬਣਾਓ"

#: src/ui/FingerprintDialog.qml:29
#, kde-format
msgid "Configure Fingerprints"
msgstr "ਫਿੰਗਰਪਰਿੰਟਾਂ ਦੀ ਸੰਰਚਨਾ"

#: src/ui/FingerprintDialog.qml:38
#, kde-format
msgctxt "@action:button 'all' refers to fingerprints"
msgid "Clear All"
msgstr "ਸਭ ਮਿਟਾਓ"

#: src/ui/FingerprintDialog.qml:45
#, kde-format
msgid "Add"
msgstr "ਜੋੜੋ"

#: src/ui/FingerprintDialog.qml:54
#, kde-format
msgid "Cancel"
msgstr "ਰੱਦ ਕਰੋ"

#: src/ui/FingerprintDialog.qml:62
#, kde-format
msgid "Done"
msgstr "ਮੁਕੰਮਲ"

#: src/ui/FingerprintDialog.qml:84
#, kde-format
msgid "Enrolling Fingerprint"
msgstr "ਫਿੰਗਰ-ਪਰਿੰਟ ਲਈ ਦਾਖਲਾ"

#: src/ui/FingerprintDialog.qml:94
#, kde-format
msgid ""
"Please repeatedly press your right index finger on the fingerprint sensor."
msgstr "ਫਿੰਗਰ-ਪਰਿੰਟ ਸੈਂਸਰ ਉੱਤੇ ਆਪਣੀ ਸੱਜੀ ਪਹਿਲੀ ਉਂਗਲ ਨੂੰ ਲਗਾਤਾਰ ਦਬਾਓ।"

#: src/ui/FingerprintDialog.qml:96
#, kde-format
msgid ""
"Please repeatedly press your right middle finger on the fingerprint sensor."
msgstr "ਫਿੰਗਰ-ਪਰਿੰਟ ਸੈਂਸਰ ਉੱਤੇ ਆਪਣੀ ਸੱਜੀ ਵੱਡੀ ਉਂਗਲ ਨੂੰ ਲਗਾਤਾਰ ਦਬਾਓ।"

#: src/ui/FingerprintDialog.qml:98
#, kde-format
msgid ""
"Please repeatedly press your right ring finger on the fingerprint sensor."
msgstr "ਫਿੰਗਰ-ਪਰਿੰਟ ਸੈਂਸਰ ਉੱਤੇ ਆਪਣੀ ਸੱਜੀ ਤੀਜੀ ਉਂਗਲ ਨੂੰ ਲਗਾਤਾਰ ਦਬਾਓ।"

#: src/ui/FingerprintDialog.qml:100
#, kde-format
msgid ""
"Please repeatedly press your right little finger on the fingerprint sensor."
msgstr "ਫਿੰਗਰ-ਪਰਿੰਟ ਸੈਂਸਰ ਉੱਤੇ ਆਪਣੀ ਸੱਜੀ ਚੀਚੀ ਨੂੰ ਲਗਾਤਾਰ ਦਬਾਓ।"

#: src/ui/FingerprintDialog.qml:102
#, kde-format
msgid "Please repeatedly press your right thumb on the fingerprint sensor."
msgstr "ਫਿੰਗਰ-ਪਰਿੰਟ ਸੈਂਸਰ ਉੱਤੇ ਆਪਣੇ ਸੱਜੇ ਅੰਗੂਠੇ ਨੂੰ ਲਗਾਤਾਰ ਦਬਾਓ।"

#: src/ui/FingerprintDialog.qml:104
#, kde-format
msgid ""
"Please repeatedly press your left index finger on the fingerprint sensor."
msgstr "ਫਿੰਗਰ-ਪਰਿੰਟ ਸੈਂਸਰ ਉੱਤੇ ਆਪਣੀ ਖੱਬੀ ਪਹਿਲੀ ਉਂਗਲ ਨੂੰ ਲਗਾਤਾਰ ਦਬਾਓ।"

#: src/ui/FingerprintDialog.qml:106
#, kde-format
msgid ""
"Please repeatedly press your left middle finger on the fingerprint sensor."
msgstr "ਫਿੰਗਰ-ਪਰਿੰਟ ਸੈਂਸਰ ਉੱਤੇ ਆਪਣੀ ਖੱਬੀ ਵੱਡੀ ਉਂਗਲ ਨੂੰ ਲਗਾਤਾਰ ਦਬਾਓ।"

#: src/ui/FingerprintDialog.qml:108
#, kde-format
msgid ""
"Please repeatedly press your left ring finger on the fingerprint sensor."
msgstr "ਫਿੰਗਰ-ਪਰਿੰਟ ਸੈਂਸਰ ਉੱਤੇ ਆਪਣੀ ਖੱਬੀ ਤੀਜੀ ਉਂਗਲ ਨੂੰ ਲਗਾਤਾਰ ਦਬਾਓ।"

#: src/ui/FingerprintDialog.qml:110
#, kde-format
msgid ""
"Please repeatedly press your left little finger on the fingerprint sensor."
msgstr "ਫਿੰਗਰ-ਪਰਿੰਟ ਸੈਂਸਰ ਉੱਤੇ ਆਪਣੀ ਖੱਬੀ ਚੀਚੀ ਨੂੰ ਲਗਾਤਾਰ ਦਬਾਓ।"

#: src/ui/FingerprintDialog.qml:112
#, kde-format
msgid "Please repeatedly press your left thumb on the fingerprint sensor."
msgstr "ਫਿੰਗਰ-ਪਰਿੰਟ ਸੈਂਸਰ ਉੱਤੇ ਆਪਣੇ ਖੱਬੇ ਅੰਗੂਠੇ ਨੂੰ ਲਗਾਤਾਰ ਦਬਾਓ।"

#: src/ui/FingerprintDialog.qml:116
#, kde-format
msgid ""
"Please repeatedly swipe your right index finger on the fingerprint sensor."
msgstr "ਫਿੰਗਰ-ਪਰਿੰਟ ਸੈਂਸਰ ਉੱਤੇ ਆਪਣੀ ਸੱਜੀ ਪਹਿਲੀ ਉਂਗਲ ਨੂੰ ਲਗਾਤਾਰ ਘਸਾਓ।"

#: src/ui/FingerprintDialog.qml:118
#, kde-format
msgid ""
"Please repeatedly swipe your right middle finger on the fingerprint sensor."
msgstr "ਫਿੰਗਰ-ਪਰਿੰਟ ਸੈਂਸਰ ਉੱਤੇ ਆਪਣੀ ਸੱਜੀ ਵੱਡੀ ਉਂਗਲ ਨੂੰ ਲਗਾਤਾਰ ਘਸਾਓ।"

#: src/ui/FingerprintDialog.qml:120
#, kde-format
msgid ""
"Please repeatedly swipe your right ring finger on the fingerprint sensor."
msgstr "ਫਿੰਗਰ-ਪਰਿੰਟ ਸੈਂਸਰ ਉੱਤੇ ਆਪਣੀ ਸੱਜੀ ਤੀਜੀ ਉਂਗਲ ਨੂੰ ਲਗਾਤਾਰ ਘਸਾਓ।"

#: src/ui/FingerprintDialog.qml:122
#, kde-format
msgid ""
"Please repeatedly swipe your right little finger on the fingerprint sensor."
msgstr "ਫਿੰਗਰ-ਪਰਿੰਟ ਸੈਂਸਰ ਉੱਤੇ ਆਪਣੀ ਸੱਜੀ ਚੀਚੀ ਨੂੰ ਲਗਾਤਾਰ ਘਸਾਓ।"

#: src/ui/FingerprintDialog.qml:124
#, kde-format
msgid "Please repeatedly swipe your right thumb on the fingerprint sensor."
msgstr "ਫਿੰਗਰ-ਪਰਿੰਟ ਸੈਂਸਰ ਉੱਤੇ ਆਪਣੇ ਸੱਜੇ ਅੰਗੂਠੇ ਨੂੰ ਲਗਾਤਾਰ ਘਸਾਓ।"

#: src/ui/FingerprintDialog.qml:126
#, kde-format
msgid ""
"Please repeatedly swipe your left index finger on the fingerprint sensor."
msgstr "ਫਿੰਗਰ-ਪਰਿੰਟ ਸੈਂਸਰ ਉੱਤੇ ਆਪਣੀ ਖੱਬੀ ਪਹਿਲੀ ਉਂਗਲ ਨੂੰ ਲਗਾਤਾਰ ਘਸਾਓ।"

#: src/ui/FingerprintDialog.qml:128
#, kde-format
msgid ""
"Please repeatedly swipe your left middle finger on the fingerprint sensor."
msgstr "ਫਿੰਗਰ-ਪਰਿੰਟ ਸੈਂਸਰ ਉੱਤੇ ਆਪਣੀ ਖੱਬੀ ਵੱਡੀ ਉਂਗਲ ਨੂੰ ਲਗਾਤਾਰ ਘਸਾਓ।"

#: src/ui/FingerprintDialog.qml:130
#, kde-format
msgid ""
"Please repeatedly swipe your left ring finger on the fingerprint sensor."
msgstr "ਫਿੰਗਰ-ਪਰਿੰਟ ਸੈਂਸਰ ਉੱਤੇ ਆਪਣੀ ਖੱਬੀ ਤੀਜੀ ਉਂਗਲ ਨੂੰ ਲਗਾਤਾਰ ਘਸਾਓ।"

#: src/ui/FingerprintDialog.qml:132
#, kde-format
msgid ""
"Please repeatedly swipe your left little finger on the fingerprint sensor."
msgstr "ਫਿੰਗਰ-ਪਰਿੰਟ ਸੈਂਸਰ ਉੱਤੇ ਆਪਣੀ ਖੱਬੀ ਚੀਚੀ ਨੂੰ ਲਗਾਤਾਰ ਘਸਾਓ।"

#: src/ui/FingerprintDialog.qml:134
#, kde-format
msgid "Please repeatedly swipe your left thumb on the fingerprint sensor."
msgstr "ਫਿੰਗਰ-ਪਰਿੰਟ ਸੈਂਸਰ ਉੱਤੇ ਆਪਣੇ ਖੱਬੇ ਅੰਗੂਠੇ ਨੂੰ ਲਗਾਤਾਰ ਘਸਾਓ।"

#: src/ui/FingerprintDialog.qml:150
#, kde-format
msgid "Finger Enrolled"
msgstr "ਫਿੰਗਰ ਦਾਖਲ ਹੋਈ"

#: src/ui/FingerprintDialog.qml:185
#, kde-format
msgid "Pick a finger to enroll"
msgstr "ਦਾਖਲ ਕਰਨ ਲਈ ਉਂਗਲ ਚੱਕੋ"

#: src/ui/FingerprintDialog.qml:304
#, kde-format
msgid "Re-enroll finger"
msgstr "ਉਂਗਲ ਮੁੜ ਦਾਖਲ ਕਰੋ"

#: src/ui/FingerprintDialog.qml:311
#, kde-format
msgid "Delete fingerprint"
msgstr "ਫਿੰਗਰ-ਪਰਿੰਟ ਹਟਾਓ"

#: src/ui/FingerprintDialog.qml:320
#, kde-format
msgid "No fingerprints added"
msgstr "ਕੋਈ ਫਿੰਗਰ-ਪਰਿੰਟ ਨਹੀਂ ਜੋੜੇ"

#: src/ui/main.qml:20
#, kde-format
msgid "Users"
msgstr "ਵਰਤੋਂਕਾਰ"

#: src/ui/main.qml:31
#, kde-format
msgctxt "@action:button As in, 'add new user'"
msgid "Add New"
msgstr "ਨਵਾਂ ਜੋੜੋ"

#: src/ui/main.qml:107
#, kde-format
msgctxt "@info:usagetip"
msgid "Press Space to edit the user profile"
msgstr "ਵਰਤੋਂਕਾਰ ਦੇ ਪਰੋਫਾਇਲ ਨੂੰ ਸੋਧਣ ਲਈ ਸਪੇਸ ਦੱਬੋ"

#: src/ui/PicturesSheet.qml:18
#, kde-format
msgctxt "@title"
msgid "Change Avatar"
msgstr "ਅਵਤਾਰ ਬਦਲੋ"

#: src/ui/PicturesSheet.qml:22
#, kde-format
msgctxt "@item:intable"
msgid "It's Nothing"
msgstr "ਇਹ ਕੁਝ ਨਹੀਂ ਹੈ"

#: src/ui/PicturesSheet.qml:23
#, kde-format
msgctxt "@item:intable"
msgid "Feisty Flamingo"
msgstr ""

#: src/ui/PicturesSheet.qml:24
#, kde-format
msgctxt "@item:intable"
msgid "Dragon's Fruit"
msgstr "ਡਰੈਨ ਫ਼ਲ"

#: src/ui/PicturesSheet.qml:25
#, kde-format
msgctxt "@item:intable"
msgid "Sweet Potato"
msgstr "ਮਿੱਠਾ ਆਲੂ"

#: src/ui/PicturesSheet.qml:26
#, kde-format
msgctxt "@item:intable"
msgid "Ambient Amber"
msgstr ""

#: src/ui/PicturesSheet.qml:27
#, kde-format
msgctxt "@item:intable"
msgid "Sparkle Sunbeam"
msgstr ""

#: src/ui/PicturesSheet.qml:28
#, kde-format
msgctxt "@item:intable"
msgid "Lemon-Lime"
msgstr ""

#: src/ui/PicturesSheet.qml:29
#, kde-format
msgctxt "@item:intable"
msgid "Verdant Charm"
msgstr ""

#: src/ui/PicturesSheet.qml:30
#, kde-format
msgctxt "@item:intable"
msgid "Mellow Meadow"
msgstr ""

#: src/ui/PicturesSheet.qml:31
#, kde-format
msgctxt "@item:intable"
msgid "Tepid Teal"
msgstr ""

#: src/ui/PicturesSheet.qml:32
#, kde-format
msgctxt "@item:intable"
msgid "Plasma Blue"
msgstr "ਪਲਾਜ਼ਮਾ ਨੀਲਾ"

#: src/ui/PicturesSheet.qml:33
#, kde-format
msgctxt "@item:intable"
msgid "Pon Purple"
msgstr ""

#: src/ui/PicturesSheet.qml:34
#, kde-format
msgctxt "@item:intable"
msgid "Bajo Purple"
msgstr ""

#: src/ui/PicturesSheet.qml:35
#, kde-format
msgctxt "@item:intable"
msgid "Burnt Charcoal"
msgstr "ਸੜਿਆ ਚਾਰਕੋਲ"

#: src/ui/PicturesSheet.qml:36
#, kde-format
msgctxt "@item:intable"
msgid "Paper Perfection"
msgstr ""

#: src/ui/PicturesSheet.qml:37
#, kde-format
msgctxt "@item:intable"
msgid "Cafétera Brown"
msgstr ""

#: src/ui/PicturesSheet.qml:38
#, kde-format
msgctxt "@item:intable"
msgid "Rich Hardwood"
msgstr "ਰਿਚ ਹਾਰਡਵੁੱਡ"

#: src/ui/PicturesSheet.qml:60
#, kde-format
msgctxt "@action:button"
msgid "Go Back"
msgstr "ਪਿੱਛੇ ਜਾਓ"

#: src/ui/PicturesSheet.qml:78
#, kde-format
msgctxt "@action:button"
msgid "Initials"
msgstr "ਸ਼ੁਰੂਆਤੀ"

#: src/ui/PicturesSheet.qml:132
#, kde-format
msgctxt "@action:button"
msgid "Choose File…"
msgstr "…ਫਾਇਲ ਚੁਣੋ"

#: src/ui/PicturesSheet.qml:137
#, kde-format
msgctxt "@title"
msgid "Choose a picture"
msgstr "ਤਸਵੀਰ ਚੁਣੋ"

#: src/ui/PicturesSheet.qml:185
#, kde-format
msgctxt "@action:button"
msgid "Placeholder Icon"
msgstr "ਪਲੇਸਹੋਲਡਰ ਆਈਕਾਨ"

#: src/ui/PicturesSheet.qml:274
#, kde-format
msgctxt "@info:whatsthis"
msgid "User avatar placeholder icon"
msgstr "ਵਰਤੋਂਕਾਰ ਅਵਤਾਰ ਥਾਂ ਆਈਕਾਨ"

#: src/ui/UserDetailsPage.qml:111
#, kde-format
msgid "Change avatar"
msgstr "ਅਵਤਾਰ ਬਦਲੋ"

#: src/ui/UserDetailsPage.qml:164
#, kde-format
msgid "Email address:"
msgstr "ਈਮੇਲ ਐਡਰੈਸ:"

#: src/ui/UserDetailsPage.qml:193
#, kde-format
msgid "Delete files"
msgstr "ਫ਼ਾਇਲਾਂ ਨੂੰ ਹਟਾਓ"

#: src/ui/UserDetailsPage.qml:200
#, kde-format
msgid "Keep files"
msgstr "ਫਾਇਲਾਂ ਨੂੰ ਰੱਖੋ"

#: src/ui/UserDetailsPage.qml:207
#, kde-format
msgid "Delete User…"
msgstr "…ਵਰਤੋਂਕਾਰ ਨੂੰ ਹਟਾਓ"

#: src/ui/UserDetailsPage.qml:219
#, kde-format
msgid "Configure Fingerprint Authentication…"
msgstr "…ਫਿੰਗਰ-ਪਰਿੰਟ ਪਰਮਾਣਕਿਤਾ ਦੀ ਸੰਰਚਨਾ"

#: src/ui/UserDetailsPage.qml:248
#, kde-kuit-format
msgctxt "@info"
msgid ""
"Fingerprints can be used in place of a password when unlocking the screen "
"and providing administrator permissions to applications and command-line "
"programs that request them.<nl/><nl/>Logging into the system with your "
"fingerprint is not yet supported."
msgstr ""
"ਸਕਰੀਨ ਨੂੰ ਅਣ-ਲਾਕ ਕਰਨ ਅਤੇ ਐਪਲੀਕੇਸ਼ਨਾਂ ਤੇ ਕਮਾਂਡ ਲਾਈਨ ਪਰੋਗਰਾਮਾਂ ਵਲੋਂ ਮੰਗ ਕਰਨ ਉੱਤੇ ਉਹਨਾਂ ਨੂੰ "
"ਪਰਸ਼ਾਸ਼ਕੀ ਇਜਾਜ਼ਤਾਂ ਦੇਣ ਲਈ ਪਾਸਵਰਡ ਦੀ ਥਾਂ ਉੱਤੇ ਫਿੰਗਰ-ਪਰਿੰਟਾਂ ਨੂੰ ਵਰਤਿਆ ਜਾ ਸਕਦਾ ਹੈ।<nl/><nl/"
">ਸਿਸਟਮ ਵਿੱਚ ਤੁਹਾਡੇ ਫਿੰਗਰਪਰਿੰਟ ਨਾਲ ਲਾਗਇਨ ਕਰਨਾ ਹਾਲੇ ਸਹਾਇਕ ਨਹੀਂ ਹੈ।"

#: src/user.cpp:290
#, kde-format
msgid "Could not get permission to save user %1"
msgstr "ਸੰਭਾਲੇ ਹੋਏ ਵਰਤੋਂਕਾਰ %1 ਲਈ ਇਜਾਜ਼ਤ ਲਈ ਨਹੀਂ ਜਾ ਸਕੀ"

#: src/user.cpp:295
#, kde-format
msgid "There was an error while saving changes"
msgstr "ਤਬਦੀਲੀਆਂ ਸੰਭਾਲਣ ਦੌਰਾਨ ਗਲਤੀ ਆਈ ਹੈ"

#: src/user.cpp:394
#, kde-format
msgid "Failed to resize image: opening temp file failed"
msgstr "ਚਿੱਤਰ ਦਾ ਆਕਾਰ ਬਦਲਣ ਲਈ ਫੇਲ੍ਹ: ਆਰਜ਼ੀ ਫਾਇਲ ਖੋਲ੍ਹਣ ਲਈ ਫੇਲ੍ਹ"

#: src/user.cpp:402
#, kde-format
msgid "Failed to resize image: writing to temp file failed"
msgstr "ਚਿੱਤਰ ਦਾ ਆਕਾਰ ਬਦਲਣ ਲਈ ਫੇਲ੍ਹ: ਆਰਜ਼ੀ ਫਾਇਲ ਲਿਖਣ ਲਈ ਫੇਲ੍ਹ"

#: src/usermodel.cpp:147
#, kde-format
msgid "Your Account"
msgstr "ਤੁਹਾਡਾ ਖਾਤਾ"

#: src/usermodel.cpp:147
#, kde-format
msgid "Other Accounts"
msgstr "ਹੋਰ ਖਾਤੇ"

#~ msgid "Manage Users"
#~ msgstr "ਵਰਤੋਂਕਾਰਾਂ ਦਾ ਇੰਤਜ਼ਾਮ"

#~ msgctxt "Example email address"
#~ msgid "john.doe@kde.org"
#~ msgstr "john.doe@kde.org"

#~ msgctxt "NAME OF TRANSLATORS"
#~ msgid "Your names"
#~ msgstr "ਅਮਨਪਰੀਤ ਸਿੰਘ ਆਲਮ ੨੦੨੧"

#~ msgctxt "EMAIL OF TRANSLATORS"
#~ msgid "Your emails"
#~ msgstr "alam.yellow@gmail.com"

#~ msgid "Manage user accounts"
#~ msgstr "ਵਰਤੋਂਕਾਰ ਖਾਤਿਆਂ ਦਾ ਇੰਤਜ਼ਾਮ ਕਰੋ"

#~ msgctxt "Example name"
#~ msgid "John Doe"
#~ msgstr "ਜੌਨ ਡੋਅ"
