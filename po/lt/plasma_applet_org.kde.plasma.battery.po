# translation of plasma_applet_battery.po to Lithuanian
# This file is distributed under the same license as the plasma_applet_battery package.
# Gintautas Miselis <gintautas@miselis.lt>, 2008.
# Donatas Glodenis <dgvirtual@akl.lt>, 2009.
# Tomas Straupis <tomasstraupis@gmail.com>, 2011.
# Remigijus Jarmalavičius <remigijus@jarmalavicius.lt>, 2011.
# Liudas Ališauskas <liudas.alisauskas@gmail.com>, 2012.
# Liudas Alisauskas <liudas@akmc.lt>, 2013, 2015.
# liudas@aksioma.lt <liudas@aksioma.lt>, 2014.
# Mindaugas Baranauskas <opensuse.lietuviu.kalba@gmail.com>, 2017.
msgid ""
msgstr ""
"Project-Id-Version: plasma_applet_battery\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2024-04-06 00:39+0000\n"
"PO-Revision-Date: 2024-03-27 23:08+0200\n"
"Last-Translator: Moo <<>>\n"
"Language-Team: Lithuanian <kde-i18n-lt@kde.org>\n"
"Language: lt\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=4; plural=(n==1 ? 0 : n%10>=2 && (n%100<10 || n"
"%100>=20) ? 1 : n%10==0 || (n%100>10 && n%100<20) ? 2 : 3);\n"
"X-Generator: Poedit 3.4.2\n"

#: package/contents/ui/BatteryItem.qml:107
#, kde-format
msgctxt "Placeholder is battery percentage"
msgid "%1%"
msgstr "%1%"

#: package/contents/ui/BatteryItem.qml:171
#, kde-format
msgid ""
"This battery's health is at only %1% and it should be replaced. Contact the "
"manufacturer."
msgstr ""
"Šio akumuliatoriaus naudingumas yra tik %1%, todėl jis turėtų būti "
"pakeistas. Susisiekite su gamintoju."

#: package/contents/ui/BatteryItem.qml:191
#, kde-format
msgid "Time To Full:"
msgstr "Laikas iki pilno įkrovimo:"

#: package/contents/ui/BatteryItem.qml:192
#, kde-format
msgid "Remaining Time:"
msgstr "Liko laiko:"

#: package/contents/ui/BatteryItem.qml:197
#, kde-format
msgctxt "@info"
msgid "Estimating…"
msgstr "Apskaičiuojama…"

#: package/contents/ui/BatteryItem.qml:209
#, kde-format
msgid "Battery Health:"
msgstr "Akumuliatoriaus naudingumas:"

#: package/contents/ui/BatteryItem.qml:215
#, kde-format
msgctxt "Placeholder is battery health percentage"
msgid "%1%"
msgstr "%1%"

#: package/contents/ui/BatteryItem.qml:229
#, kde-format
msgid "Battery is configured to charge up to approximately %1%."
msgstr "Jūsų akumuliatorius yra sukonfigūruotas įsikrauti iki maždaug %1%."

#: package/contents/ui/CompactRepresentation.qml:111
#, kde-format
msgctxt "battery percentage below battery icon"
msgid "%1%"
msgstr "%1%"

#: package/contents/ui/logic.js:24 package/contents/ui/logic.js:30
#: package/contents/ui/main.qml:228
#, kde-format
msgid "Fully Charged"
msgstr "Pilnai įkrautas"

#: package/contents/ui/logic.js:29
#, kde-format
msgid "Discharging"
msgstr "Išsikrauna"

#: package/contents/ui/logic.js:31
#, kde-format
msgid "Charging"
msgstr "Įkraunamas"

#: package/contents/ui/logic.js:33
#, kde-format
msgid "Not Charging"
msgstr "Nekraunamas"

#: package/contents/ui/logic.js:36
#, kde-format
msgctxt "Battery is currently not present in the bay"
msgid "Not present"
msgstr "Nėra"

#: package/contents/ui/main.qml:121
#, kde-format
msgid "The battery applet has enabled system-wide inhibition"
msgstr "Akumuliatoriaus programėlė įjungė sulaikymą sistemos mastu"

#: package/contents/ui/main.qml:133
#, kde-format
msgid "Failed to block automatic sleep and screen locking"
msgstr "Nepavyko blokuoti automatinio pristabdymo ir ekrano užrakinimo"

#: package/contents/ui/main.qml:147
#, kde-format
msgid "Failed to unblock automatic sleep and screen locking"
msgstr "Nepavyko atblokuoti automatinio pristabdymo ir ekrano užrakinimo"

#: package/contents/ui/main.qml:159 package/contents/ui/main.qml:197
#: package/contents/ui/main.qml:203
#, kde-format
msgid "Power Management"
msgstr "Energijos valdymas"

#: package/contents/ui/main.qml:176
#, kde-format
msgid "Failed to activate %1 mode"
msgstr "Nepavyko aktyvuoti veiksenos %1"

#: package/contents/ui/main.qml:203
#, kde-format
msgid "Power and Battery"
msgstr "Maitinimas ir akumuliatorius"

#: package/contents/ui/main.qml:235
#, kde-format
msgid "Battery at %1%, not Charging"
msgstr "Akumuliatorius ties %1%, nekraunamas"

#: package/contents/ui/main.qml:237
#, kde-format
msgid "Battery at %1%, plugged in but still discharging"
msgstr "Akumuliatorius ties %1%, prijungtas, bet vis tiek išsikrauna"

#: package/contents/ui/main.qml:239
#, kde-format
msgid "Battery at %1%, Charging"
msgstr "Akumuliatorius ties %1%, įkraunamas"

#: package/contents/ui/main.qml:242
#, kde-format
msgid "Battery at %1%"
msgstr "Akumuliatorius ties %1%"

#: package/contents/ui/main.qml:250
#, kde-format
msgid "The power supply is not powerful enough to charge the battery"
msgstr "Maitinimo šaltinis nėra pakankamai galingas, kad įkrautų akumuliatorių"

#: package/contents/ui/main.qml:254
#, kde-format
msgid "No Batteries Available"
msgstr "Nėra jokių prieinamų akumuliatorių"

#: package/contents/ui/main.qml:260
#, kde-format
msgctxt "time until fully charged - HH:MM"
msgid "%1 until fully charged"
msgstr "%1 iki pilnai įkrauto"

#: package/contents/ui/main.qml:262
#, kde-format
msgctxt "remaining time left of battery usage - HH:MM"
msgid "%1 remaining"
msgstr "Liko %1"

#: package/contents/ui/main.qml:265
#, kde-format
msgid "Not charging"
msgstr "Nekraunamas"

#: package/contents/ui/main.qml:269
#, kde-format
msgid ""
"Automatic sleep and screen locking are disabled; middle-click to re-enable"
msgstr ""
"Automatinis pristabdymas ir ekrano užrakinimas yra išjungti; spustelėkite "
"viduriniuoju pelės mygtuku norėdami įjungti iš naujo"

#: package/contents/ui/main.qml:271
#, kde-format
msgid "Middle-click to disable automatic sleep and screen locking"
msgstr ""
"Spustelėkite viduriniuoju pelės mygtuku norėdami išjungti automatinį "
"pristabdymą ir ekrano užrakinimą"

#: package/contents/ui/main.qml:276
#, kde-format
msgid "An application has requested activating Performance mode"
msgid_plural "%1 applications have requested activating Performance mode"
msgstr[0] "Programa paprašė aktyvuoti našumo veikseną"
msgstr[1] "%1 programos paprašė aktyvuoti našumo veikseną"
msgstr[2] "%1 programų paprašė aktyvuoti našumo veikseną"
msgstr[3] "%1 programa paprašė aktyvuoti našumo veikseną"

#: package/contents/ui/main.qml:280
#, kde-format
msgid "System is in Performance mode; scroll to change"
msgstr "Sistema yra našumo veiksenoje; slinkite norėdami pakeisti"

#: package/contents/ui/main.qml:284
#, kde-format
msgid "An application has requested activating Power Save mode"
msgid_plural "%1 applications have requested activating Power Save mode"
msgstr[0] "Programa paprašė aktyvuoti energijos taupymo veikseną"
msgstr[1] "%1 programos paprašė aktyvuoti energijos taupymo veikseną"
msgstr[2] "%1 programų paprašė aktyvuoti energijos taupymo veikseną"
msgstr[3] "%1 programa paprašė aktyvuoti energijos taupymo veikseną"

#: package/contents/ui/main.qml:288
#, kde-format
msgid "System is in Power Save mode; scroll to change"
msgstr "Sistema yra energijos taupymo veiksenoje; slinkite norėdami pakeisti"

#: package/contents/ui/main.qml:291
#, kde-format
msgid "System is in Balanced Power mode; scroll to change"
msgstr ""
"Sistema yra subalansuotoje energijos veiksenoje; slinkite norėdami pakeisti"

#: package/contents/ui/main.qml:383
#, kde-format
msgid "&Show Energy Information…"
msgstr "&Rodyti energijos informaciją…"

#: package/contents/ui/main.qml:389
#, kde-format
msgid "Show Battery Percentage on Icon When Not Fully Charged"
msgstr ""
"Kai akumuliatorius nėra pilnai įkrautas, rodyti piktogramoje jo įkrovos "
"procentinę dalį"

#: package/contents/ui/main.qml:402
#, kde-format
msgid "&Configure Power Management…"
msgstr "&Konfigūruoti energijos valdymą…"

#: package/contents/ui/PowerManagementItem.qml:41
#, kde-format
msgctxt "Minimize the length of this string as much as possible"
msgid "Manually block sleep and screen locking"
msgstr "Rankiniu būdu blokuoti pristabdymą ir ekrano užrakinimą"

#: package/contents/ui/PowerManagementItem.qml:76
#, kde-format
msgctxt "Minimize the length of this string as much as possible"
msgid ""
"Your laptop is configured not to sleep when closing the lid while an "
"external monitor is connected."
msgstr ""
"Jūsų įrenginys sukonfigūruotas taip, kad uždarius dangtį, kol yra prijungtas "
"išorinis monitorius, kompiuteris nebūtų pristabdomas."

#: package/contents/ui/PowerManagementItem.qml:87
#, kde-format
msgid "%1 application is currently blocking sleep and screen locking:"
msgid_plural "%1 applications are currently blocking sleep and screen locking:"
msgstr[0] ""
"Šiuo metu %1 programa blokuoja kompiuterio pristabdymą ir ekrano užrakinimą:"
msgstr[1] ""
"Šiuo metu %1 programos blokuoja kompiuterio pristabdymą ir ekrano užrakinimą:"
msgstr[2] ""
"Šiuo metu %1 programų blokuoja kompiuterio pristabdymą ir ekrano užrakinimą:"
msgstr[3] ""
"Šiuo metu %1 programa blokuoja kompiuterio pristabdymą ir ekrano užrakinimą:"

#: package/contents/ui/PowerManagementItem.qml:107
#, kde-format
msgid "%1 is currently blocking sleep and screen locking (%2)"
msgstr ""
"Šiuo metu %1 blokuoja kompiuterio pristabdymą ir ekrano užrakinimą (%2)"

#: package/contents/ui/PowerManagementItem.qml:109
#, kde-format
msgid "%1 is currently blocking sleep and screen locking (unknown reason)"
msgstr ""
"Šiuo metu %1 blokuoja kompiuterio pristabdymą ir ekrano užrakinimą "
"(priežastis nežinoma)"

#: package/contents/ui/PowerManagementItem.qml:111
#, kde-format
msgid "An application is currently blocking sleep and screen locking (%1)"
msgstr ""
"Šiuo metu programa blokuoja kompiuterio pristabdymą ir ekrano užrakinimą (%1)"

#: package/contents/ui/PowerManagementItem.qml:113
#, kde-format
msgid ""
"An application is currently blocking sleep and screen locking (unknown "
"reason)"
msgstr ""
"Šiuo metu programa blokuoja kompiuterio pristabdymą ir ekrano užrakinimą "
"(priežastis nežinoma)"

#: package/contents/ui/PowerManagementItem.qml:117
#, kde-format
msgctxt "Application name: reason for preventing sleep and screen locking"
msgid "%1: %2"
msgstr "%1: %2"

#: package/contents/ui/PowerManagementItem.qml:119
#, kde-format
msgctxt "Application name: reason for preventing sleep and screen locking"
msgid "%1: unknown reason"
msgstr "%1: priežastis nežinoma"

#: package/contents/ui/PowerManagementItem.qml:121
#, kde-format
msgctxt "Application name: reason for preventing sleep and screen locking"
msgid "Unknown application: %1"
msgstr "Nežinoma programa: %1"

#: package/contents/ui/PowerManagementItem.qml:123
#, kde-format
msgctxt "Application name: reason for preventing sleep and screen locking"
msgid "Unknown application: unknown reason"
msgstr "Nežinoma programa: priežastis nežinoma"

#: package/contents/ui/PowerProfileItem.qml:37
#, kde-format
msgid "Power Save"
msgstr "Energijos taupymas"

#: package/contents/ui/PowerProfileItem.qml:41
#, kde-format
msgid "Balanced"
msgstr "Subalansuotas"

#: package/contents/ui/PowerProfileItem.qml:45
#, kde-format
msgid "Performance"
msgstr "Našumas"

#: package/contents/ui/PowerProfileItem.qml:62
#, kde-format
msgid "Power Profile"
msgstr "Maitinimo profilis"

#: package/contents/ui/PowerProfileItem.qml:97
#, kde-format
msgctxt "Power profile"
msgid "Not available"
msgstr "Neprieinamas"

#: package/contents/ui/PowerProfileItem.qml:192
#, kde-format
msgid ""
"Performance mode has been disabled to reduce heat generation because the "
"computer has detected that it may be sitting on your lap."
msgstr ""
"Našumo veiksena išjungta, kad būtų sumažinta šilumos gamyba, nes kompiuteris "
"aptiko, kad jis, galimai, guli jums ant kelių."

#: package/contents/ui/PowerProfileItem.qml:194
#, kde-format
msgid ""
"Performance mode is unavailable because the computer is running too hot."
msgstr "Našumo veiksena yra neprieinama, nes kompiuteris per daug įkaito."

#: package/contents/ui/PowerProfileItem.qml:196
#, kde-format
msgid "Performance mode is unavailable."
msgstr "Našumo veiksena neprieinama."

#: package/contents/ui/PowerProfileItem.qml:209
#, kde-format
msgid ""
"Performance may be lowered to reduce heat generation because the computer "
"has detected that it may be sitting on your lap."
msgstr ""
"Našumas gali būti sumažintas, kad būtų sumažinta šilumos gamyba, nes "
"kompiuteris aptiko, kad jis, galimai, guli jums ant kelių."

#: package/contents/ui/PowerProfileItem.qml:211
#, kde-format
msgid "Performance may be reduced because the computer is running too hot."
msgstr "Našumas gali būti sumažintas, nes kompiuteris per daug įkaito."

#: package/contents/ui/PowerProfileItem.qml:213
#, kde-format
msgid "Performance may be reduced."
msgstr "Gali sumažėti našumas."

#: package/contents/ui/PowerProfileItem.qml:224
#, kde-format
msgid "One application has requested activating %2:"
msgid_plural "%1 applications have requested activating %2:"
msgstr[0] "Viena programa paprašė, kad būtų aktyvuotas %2:"
msgstr[1] "%1 programos paprašė, kad būtų aktyvuotas %2:"
msgstr[2] "%1 programų paprašė, kad būtų aktyvuotas %2:"
msgstr[3] "%1 programa paprašė, kad būtų aktyvuotas %2:"

#: package/contents/ui/PowerProfileItem.qml:242
#, kde-format
msgctxt ""
"%1 is the name of the application, %2 is the reason provided by it for "
"activating performance mode"
msgid "%1: %2"
msgstr "%1: %2"

#: package/contents/ui/PowerProfileItem.qml:261
#, kde-kuit-format
msgid ""
"Power profiles may be supported on your device.<nl/>Try installing the "
"<command>power-profiles-daemon</command> package using your distribution's "
"package manager and restarting the system."
msgstr ""
"Jūsų įrenginyje gali būti palaikomi maitinimo profiliai.<nl/>Pabandykite "
"naudodami savo platinamojo paketo paketų tvarkytuvę įdiegti paketą "
"<command>power-profiles-daemon</command> ir paleiskite sistemą iš naujo."

#~ msgctxt "Placeholder is brightness percentage"
#~ msgid "%1%"
#~ msgstr "%1%"

#~ msgid "Battery and Brightness"
#~ msgstr "Akumuliatorius ir ryškumas"

#~ msgid "Brightness"
#~ msgstr "Ryškumas"

#~ msgid "Battery"
#~ msgstr "Akumuliatorius"

#~ msgid "Scroll to adjust screen brightness"
#~ msgstr "Slinkite norėdami derinti ekrano ryškumą"

#~ msgid "Display Brightness"
#~ msgstr "Ekrano ryškumas"

#~ msgid "Keyboard Brightness"
#~ msgstr "Klaviatūros ryškumas"

#, fuzzy
#~| msgid "Performance mode is unavailable."
#~ msgid "Performance mode has been manually enabled"
#~ msgstr "Našumo veiksena neprieinama."

#~ msgid "General"
#~ msgstr "Bendra"

#~ msgid "An application is preventing sleep and screen locking:"
#~ msgstr "Programa neleidžia pristabdyti kompiuterio ir užrakinti ekrano:"

#~ msgctxt "short symbol to signal there is no battery currently available"
#~ msgid "-"
#~ msgstr "-"

#~ msgid "Configure Power Saving..."
#~ msgstr "Konfigūruoti energijos taupymą..."

#~ msgid "Time To Empty:"
#~ msgstr "Laikas iki pilno išsikrovimo:"

#~ msgid ""
#~ "Disabling power management will prevent your screen and computer from "
#~ "turning off automatically.\n"
#~ "\n"
#~ "Most applications will automatically suppress power management when they "
#~ "don't want to have you interrupted."
#~ msgstr ""
#~ "Išjungus energijos valdymą, jūsų ekranui ir kompiuteriui nebus leidžiama "
#~ "automatiškai išsijungti.\n"
#~ "\n"
#~ "Daugelis programų, joms nenorint jūsų trukdyti, automatiškai slopins "
#~ "energijos valdymą."

#~ msgctxt "Some Application and n others are currently suppressing PM"
#~ msgid ""
#~ "%2 and %1 other application are currently suppressing power management."
#~ msgid_plural ""
#~ "%2 and %1 other applications are currently suppressing power management."
#~ msgstr[0] "Šiuo metu %2 ir dar %1 programa slopina energijos valdymą."
#~ msgstr[1] "Šiuo metu %2 ir dar %1 programos slopina energijos valdymą."
#~ msgstr[2] "Šiuo metu %2 ir dar %1 programų slopina energijos valdymą."
#~ msgstr[3] "Šiuo metu %2 ir dar %1 programa slopina energijos valdymą."

#~ msgctxt "Some Application is suppressing PM"
#~ msgid "%1 is currently suppressing power management."
#~ msgstr "Šiuo metu %1 slopina energijos valdymą."

#~ msgctxt "Some Application is suppressing PM: Reason provided by the app"
#~ msgid "%1 is currently suppressing power management: %2"
#~ msgstr "Šiuo metu %1 slopina energijos valdymą: %2"

#~ msgctxt "Used for measurement"
#~ msgid "100%"
#~ msgstr "100%"

#~ msgid "%1% Charging"
#~ msgstr "%1% Įkraunamas"

#~ msgid "%1% Plugged in"
#~ msgstr "%1% Prijungtas"

#~ msgid "%1% Battery Remaining"
#~ msgstr "Liko %1% akumuliatoriaus įkrovos"

#~ msgctxt "The degradation in the battery's energy capacity"
#~ msgid "Capacity degradation:"
#~ msgstr "Talpos smukimas:"

#~ msgctxt "Placeholder is battery's capacity degradation"
#~ msgid "%1%"
#~ msgstr "%1%"

#~ msgctxt "Placeholder is battery capacity"
#~ msgid "%1%"
#~ msgstr "%1%"

#~ msgid "Vendor:"
#~ msgstr "Tiekėjas:"

#~ msgid "Model:"
#~ msgstr "Modelis:"

#~ msgid "No screen or keyboard brightness controls available"
#~ msgstr "Nėra pasiekiamų klaviatūros apšvietimo valdiklių"

#~ msgctxt "Placeholders are current and maximum brightness step"
#~ msgid "%1/%2"
#~ msgstr "%1/%2"
