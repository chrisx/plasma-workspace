# Copyright (C) YEAR This file is copyright:
# This file is distributed under the same license as the plasma-desktop package.
#
# Giovanni Sora <g.sora@tiscali.it>, 2019, 2020, 2021, 2022, 2023.
msgid ""
msgstr ""
"Project-Id-Version: plasma-desktop\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2024-04-06 00:39+0000\n"
"PO-Revision-Date: 2023-11-30 09:11+0100\n"
"Last-Translator: giovanni <g.sora@tiscali.it>\n"
"Language-Team: Interlingua <kde-i18n-doc@kde.org>\n"
"Language: ia\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Lokalize 22.12.3\n"

#: ui/DayNightView.qml:116
#, kde-format
msgid ""
"Color temperature begins changing to night time at %1 and is fully changed "
"by %2"
msgstr ""
"Temperatura de color initia a cambiar le tempore de nocte a %1 e es "
"completemente  cambiate ab %2 "

#: ui/DayNightView.qml:119
#, kde-format
msgid ""
"Color temperature begins changing to day time at %1 and is fully changed by "
"%2"
msgstr ""
"Temperatura de color initia a cambiar le tempore del die a %1 e es "
"completemente  cambiate ab %2 "

#: ui/LocationsFixedView.qml:38
#, kde-format
msgctxt ""
"@label:chooser Tap should be translated to mean touching using a touchscreen"
msgid "Tap to choose your location on the map."
msgstr "Toca per seliger tu location sur le mappa."

#: ui/LocationsFixedView.qml:39
#, kde-format
msgctxt ""
"@label:chooser Click should be translated to mean clicking using a mouse"
msgid "Click to choose your location on the map."
msgstr "Clicca pro seliger tu location sur le mappa"

#: ui/LocationsFixedView.qml:78 ui/LocationsFixedView.qml:103
#, kde-format
msgid "Zoom in"
msgstr "Zoom in (aggrandi)"

#: ui/LocationsFixedView.qml:209
#, kde-kuit-format
msgctxt "@info"
msgid ""
"Modified from <link url='https://commons.wikimedia.org/wiki/File:"
"World_location_map_(equirectangular_180).svg'>World location map</link> by "
"TUBS / Wikimedia Commons / <link url='https://creativecommons.org/licenses/"
"by-sa/3.0'>CC BY-SA 3.0</link>"
msgstr ""
"Modificate ex <link url='https://commons.wikimedia.org/wiki/File:"
"World_location_map_(equirectangular_180).svg'>World location map</link> per "
"TUBS / Wikimedia Commons / <link url='https://creativecommons.org/licenses/"
"by-sa/3.0'>CC BY-SA 3.0</link>"

#: ui/LocationsFixedView.qml:222
#, kde-format
msgctxt "@label: textbox"
msgid "Latitude:"
msgstr "Latitude:"

#: ui/LocationsFixedView.qml:249
#, kde-format
msgctxt "@label: textbox"
msgid "Longitude:"
msgstr "Longitude:"

#: ui/main.qml:98
#, kde-format
msgid "The blue light filter makes the colors on the screen warmer."
msgstr "Filtro de blau legier face le colores sur le schermo plus calide."

#: ui/main.qml:151
#, kde-format
msgid "Day light temperature:"
msgstr "Temperatura de Color de Die:"

#: ui/main.qml:194 ui/main.qml:256
#, kde-format
msgctxt "Color temperature in Kelvin"
msgid "%1K"
msgstr "%1 K"

#: ui/main.qml:199 ui/main.qml:261
#, kde-format
msgctxt "Night colour blue-ish; no blue light filter activated"
msgid "Cool (no filter)"
msgstr "Fresc (necun filtro)"

#: ui/main.qml:206 ui/main.qml:268
#, kde-format
msgctxt "Night colour red-ish"
msgid "Warm"
msgstr "Calide"

#: ui/main.qml:213
#, kde-format
msgid "Night light temperature:"
msgstr "Temperatura de Color de Nocte:"

#: ui/main.qml:280
#, kde-format
msgid "Switching times:"
msgstr "Commuta Tempores:"

#: ui/main.qml:283
#, kde-format
msgid "Always off"
msgstr "Sempre Off"

#: ui/main.qml:284
#, kde-format
msgid "Sunset and sunrise at current location"
msgstr "Poner del sol e levar del sol al location currente"

#: ui/main.qml:285
#, kde-format
msgid "Sunset and sunrise at manual location"
msgstr "Poner del sol e levar de sol al location manual"

#: ui/main.qml:286
#, kde-format
msgid "Custom times"
msgstr "Tempores personalisate"

#: ui/main.qml:287
#, kde-format
msgid "Always on night light"
msgstr "Sempre Active sur color de nocte"

#: ui/main.qml:304
#, kde-format
msgctxt "@label The coordinates for the current location"
msgid "Current location:"
msgstr "Location currente:"

#: ui/main.qml:310
#, kde-format
msgid "Latitude: %1°   Longitude: %2°"
msgstr "Latitude: %1°   Longitude: %2°"

#: ui/main.qml:332
#, kde-kuit-format
msgctxt "@info"
msgid ""
"The device's location will be periodically updated using GPS (if available), "
"or by sending network information to <link url='https://location.services."
"mozilla.com'>Mozilla Location Service</link>."
msgstr ""
"Le location de dispositivo essera periodicamente actualisate usante GPS (si "
"disponibile), o per inviar information de rete a <link url='https://location."
"services.mozilla.com'>Mozilla Location Service</link>."

#: ui/main.qml:350
#, kde-format
msgid "Begin night light at:"
msgstr "Initia color de nocte a :"

#: ui/main.qml:363 ui/main.qml:386
#, kde-format
msgid "Input format: HH:MM"
msgstr "Formato de ingresso: HH:MM"

#: ui/main.qml:373
#, kde-format
msgid "Begin day light at:"
msgstr "Initia color de die a:"

#: ui/main.qml:395
#, kde-format
msgid "Transition duration:"
msgstr "Duration de transition:"

#: ui/main.qml:404
#, kde-format
msgid "%1 minute"
msgid_plural "%1 minutes"
msgstr[0] "%1 minuta"
msgstr[1] "%1 minutas"

#: ui/main.qml:417
#, kde-format
msgid "Input minutes - min. 1, max. 600"
msgstr "Minutas de entrata -min. 1, max. 600"

#: ui/main.qml:436
#, kde-format
msgid "Error: Transition time overlaps."
msgstr "Error: tempore de transition imbrica"

#: ui/main.qml:459
#, kde-format
msgctxt "@info:placeholder"
msgid "Locating…"
msgstr "Locante..."

#~ msgctxt "Night colour blue-ish"
#~ msgid "Cool"
#~ msgstr "Fresc"

#, fuzzy
#~| msgid "This is what Night Color will look like when active."
#~ msgid "This is what day color temperature will look like when active."
#~ msgstr "Isto es lo que Night Color (Color de Nocte) semblara quando active."

#, fuzzy
#~| msgid "This is what Night Color will look like when active."
#~ msgid "This is what night color temperature will look like when active."
#~ msgstr "Isto es lo que Night Color (Color de Nocte) semblara quando active."

#, fuzzy
#~| msgid "Activate Night Color"
#~ msgid "Activate blue light filter"
#~ msgstr "Activa Color de Nocte"

#~ msgid "Turn on at:"
#~ msgstr "Accende a:"

#~ msgid "Turn off at:"
#~ msgstr "Extingue a:"

#~ msgid "Night Color begins changing back at %1 and ends at %2"
#~ msgstr "Color de nocte initia a cambiar retro a %1 e termina a %2"

#~ msgid "Error: Morning is before evening."
#~ msgstr "Error: matino es ante vespere"

#~ msgid ""
#~ "The device's location will be detected using GPS (if available), or by "
#~ "sending network information to <a href=\"https://location.services."
#~ "mozilla.com\">Mozilla Location Service</a>."
#~ msgstr ""
#~ "Le location de dispositivo essera discoperite usante GPS (si "
#~ "disponibile), o per inviar information de rete a <a href=\"https://"
#~ "location.services.mozilla.com\">Mozilla Location Service</a>."

#~ msgid "Night Color begins at %1"
#~ msgstr "Color de nocte initia a %1"

#~ msgid "Color fully changed at %1"
#~ msgstr "Color completemente modificate a %1"

#~ msgid "Normal coloration restored by %1"
#~ msgstr "Coloration normal restabilite per %1"

#~ msgctxt "NAME OF TRANSLATORS"
#~ msgid "Your names"
#~ msgstr "Giovanni Sora"

#~ msgctxt "EMAIL OF TRANSLATORS"
#~ msgid "Your emails"
#~ msgstr "g.sora@tiscali.it"

#~ msgid "Night Color"
#~ msgstr "Color de Nocte"

#~ msgid "Roman Gilg"
#~ msgstr "Roman Gilg"

#~ msgid " K"
#~ msgstr "K"
