# Translation of kcm_nightcolor.po into Serbian.
# Chusslove Illich <caslav.ilic@gmx.net>, 2017.
msgid ""
msgstr ""
"Project-Id-Version: kcm_nightcolor\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2024-04-06 00:39+0000\n"
"PO-Revision-Date: 2017-12-17 18:00+0100\n"
"Last-Translator: Chusslove Illich <caslav.ilic@gmx.net>\n"
"Language-Team: Serbian <kde-i18n-sr@kde.org>\n"
"Language: sr@ijekavianlatin\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=4; plural=n==1 ? 3 : n%10==1 && n%100!=11 ? 0 : n"
"%10>=2 && n%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2;\n"
"X-Accelerator-Marker: &\n"
"X-Text-Markup: kde4\n"
"X-Environment: kde\n"

#: ui/DayNightView.qml:116
#, kde-format
msgid ""
"Color temperature begins changing to night time at %1 and is fully changed "
"by %2"
msgstr ""

#: ui/DayNightView.qml:119
#, kde-format
msgid ""
"Color temperature begins changing to day time at %1 and is fully changed by "
"%2"
msgstr ""

#: ui/LocationsFixedView.qml:38
#, kde-format
msgctxt ""
"@label:chooser Tap should be translated to mean touching using a touchscreen"
msgid "Tap to choose your location on the map."
msgstr ""

#: ui/LocationsFixedView.qml:39
#, kde-format
msgctxt ""
"@label:chooser Click should be translated to mean clicking using a mouse"
msgid "Click to choose your location on the map."
msgstr ""

#: ui/LocationsFixedView.qml:78 ui/LocationsFixedView.qml:103
#, kde-format
msgid "Zoom in"
msgstr ""

#: ui/LocationsFixedView.qml:209
#, kde-kuit-format
msgctxt "@info"
msgid ""
"Modified from <link url='https://commons.wikimedia.org/wiki/File:"
"World_location_map_(equirectangular_180).svg'>World location map</link> by "
"TUBS / Wikimedia Commons / <link url='https://creativecommons.org/licenses/"
"by-sa/3.0'>CC BY-SA 3.0</link>"
msgstr ""

#: ui/LocationsFixedView.qml:222
#, fuzzy, kde-format
#| msgid "Latitude"
msgctxt "@label: textbox"
msgid "Latitude:"
msgstr "Geografska širina"

#: ui/LocationsFixedView.qml:249
#, fuzzy, kde-format
#| msgid "Longitude"
msgctxt "@label: textbox"
msgid "Longitude:"
msgstr "Geografska dužina"

#: ui/main.qml:98
#, kde-format
msgid "The blue light filter makes the colors on the screen warmer."
msgstr ""

#: ui/main.qml:151
#, fuzzy, kde-format
#| msgid "Night Color Temperature: "
msgid "Day light temperature:"
msgstr "Temperatura noćne boje: "

#: ui/main.qml:194 ui/main.qml:256
#, kde-format
msgctxt "Color temperature in Kelvin"
msgid "%1K"
msgstr ""

#: ui/main.qml:199 ui/main.qml:261
#, kde-format
msgctxt "Night colour blue-ish; no blue light filter activated"
msgid "Cool (no filter)"
msgstr ""

#: ui/main.qml:206 ui/main.qml:268
#, kde-format
msgctxt "Night colour red-ish"
msgid "Warm"
msgstr ""

#: ui/main.qml:213
#, fuzzy, kde-format
#| msgid "Night Color Temperature: "
msgid "Night light temperature:"
msgstr "Temperatura noćne boje: "

#: ui/main.qml:280
#, kde-format
msgid "Switching times:"
msgstr ""

#: ui/main.qml:283
#, kde-format
msgid "Always off"
msgstr ""

#: ui/main.qml:284
#, kde-format
msgid "Sunset and sunrise at current location"
msgstr ""

#: ui/main.qml:285
#, kde-format
msgid "Sunset and sunrise at manual location"
msgstr ""

#: ui/main.qml:286
#, kde-format
msgid "Custom times"
msgstr ""

#: ui/main.qml:287
#, kde-format
msgid "Always on night light"
msgstr ""

#: ui/main.qml:304
#, fuzzy, kde-format
#| msgid "Detect location"
msgctxt "@label The coordinates for the current location"
msgid "Current location:"
msgstr "Otkrij lokaciju"

#: ui/main.qml:310
#, kde-format
msgid "Latitude: %1°   Longitude: %2°"
msgstr ""

#: ui/main.qml:332
#, kde-kuit-format
msgctxt "@info"
msgid ""
"The device's location will be periodically updated using GPS (if available), "
"or by sending network information to <link url='https://location.services."
"mozilla.com'>Mozilla Location Service</link>."
msgstr ""

#: ui/main.qml:350
#, kde-format
msgid "Begin night light at:"
msgstr ""

#: ui/main.qml:363 ui/main.qml:386
#, kde-format
msgid "Input format: HH:MM"
msgstr ""

#: ui/main.qml:373
#, kde-format
msgid "Begin day light at:"
msgstr ""

#: ui/main.qml:395
#, fuzzy, kde-format
#| msgid "Transition duration"
msgid "Transition duration:"
msgstr "Trajanje prelaza"

#: ui/main.qml:404
#, kde-format
msgid "%1 minute"
msgid_plural "%1 minutes"
msgstr[0] ""
msgstr[1] ""
msgstr[2] ""
msgstr[3] ""

# well-spelled: мин, макс
#: ui/main.qml:417
#, fuzzy, kde-format
#| msgid "(In minutes - min. 1, max. 600)"
msgid "Input minutes - min. 1, max. 600"
msgstr "(u minutima, min. 1, maks. 600)"

#: ui/main.qml:436
#, kde-format
msgid "Error: Transition time overlaps."
msgstr "Greška: vreme prelaza se preklapa."

#: ui/main.qml:459
#, kde-format
msgctxt "@info:placeholder"
msgid "Locating…"
msgstr ""

#~ msgctxt "NAME OF TRANSLATORS"
#~ msgid "Your names"
#~ msgstr "Časlav Ilić"

#~ msgctxt "EMAIL OF TRANSLATORS"
#~ msgid "Your emails"
#~ msgstr "caslav.ilic@gmx.net"

#~ msgid "Night Color"
#~ msgstr "Noćna boja"

#~ msgid "Roman Gilg"
#~ msgstr "Roman Gilg"

#~ msgid "Activate Night Color"
#~ msgstr "Aktiviraj noćnu boju"

#~ msgid " K"
#~ msgstr " K"

#, fuzzy
#~| msgid "Error: Morning not before evening."
#~ msgid "Error: Morning is before evening."
#~ msgstr "Greška: jutro nije pre večeri."

#, fuzzy
#~| msgid "Night Color Temperature: "
#~ msgid "Night Color begins at %1"
#~ msgstr "Temperatura noćne boje: "
